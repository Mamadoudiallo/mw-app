import React from 'react';
import {
  View,
} from 'react-native';

import { Root } from './src/config/Router';

export default class App extends React.Component {
  render() {
    return (
      <Root/>
    );
  }
}