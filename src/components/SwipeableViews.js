import React, { Component } from 'react';
import SwipeableViews from 'react-swipeable-views-native';

class SwipeableViews extends Component {
    render() {
        return (
            <SwipeableViews style={styles.slideContainer}>
                <View style={[styles.slide, styles.slide1]}>
                    <Text style={styles.text}>
                        slide n°1
                    </Text>
                </View>
                <View style={[styles.slide, styles.slide2]}>
                    <Text style={styles.text}>
                        slide n°2
                    </Text>
                </View>
                <View style={[styles.slide, styles.slide3]}>
                    <Text style={styles.text}>
                        slide n°3
                    </Text>
                </View>
            </SwipeableViews>
        );
    }
}

export default SwipeableViews;

const styles = StyleSheet.create({
    slideContainer: {
        height: 100,
    },
    slide: {
        padding: 15,
        height: 100,
    },
    slide1: {
        backgroundColor: '#FEA900',
    },
    slide2: {
        backgroundColor: '#B3DC4A',
    },
    slide3: {
        backgroundColor: '#6AC0FF',
    },
    text: {
        color: '#fff',
        fontSize: 16,
    },
});