import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    View,
    StyleSheet,
    Image,
} from 'react-native';

const { width, height } = Dimensions.get('window');

class AppLogin extends Component {
    constructor(props) {
        super(props);
        this.state = { username: 'Mamadou', password: 'Passw0rd' };
    }
    static navigationOptions = {
        header: null,
    };
    handlePress = () => {
        this.props.navigation.navigate('Memberarea');
    };

    login = () => {
        fetch('http:190.192.0.122', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        })
            .then((response) => response.json())
            .then((res) => {

                //if our rsponse is true, as set in Express route / json
                if (res.success === true) {
                    var username = res.message;

                    //We use AsyncStorage to store the users username
                    AsyncStorage.setItem('username', username);

                    //Then redirect to the memberarea
                    /* this.props.navigator.push({
                        id: 'memberarea'
                    }) */
                } else {
                    alert(res.message)
                }

            })
            .done()
    }
    render() {
        return (
            <View style={styles.loginContainer}>
                <Image source={require('../img/praktijkonderwijs_login.jpg')} style={styles.backgroundImage}>
                    <View style={styles.content}>
                        <Text style={styles.logo}> MUISWERK</Text>
                        <View style={styles.inputContainer}>
                            <TextInput underlineColorAndroid='transparent'
                                onChangeText={(username) => this.setState(username)}
                                value={this.state.username}
                                style={styles.input} placeholder='Username'>
                            </TextInput>

                            <TextInput secureTextEntry={true} underlineColorAndroid='transparent'
                                onChangeText={(password) => this.setState(password)}
                                value={this.state.password}
                                style={styles.input} placeholder='Password'>
                            </TextInput>

                            <TouchableOpacity onPress={this.handlePress} style={styles.buttonContainer}>
                                <Text style={styles.buttonText}>LOGIN</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Image>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loginContainer: {
        flex: 1,
        height: height
    },
    backgroundImage: {
        flex: 1,
        width: width,
        height: height,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    content: {
        alignItems: 'center'
    },
    logo: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        fontStyle: 'italic',
        textShadowColor: '#252525',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 15,
        marginBottom: 20,
    },
    inputContainer: {
        margin: 20,
        marginBottom: 0,
        padding: 20,
        paddingBottom: 10,
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(255,255,255,0.2)',
    },
    input: {
        fontSize: 16,
        height: 40,
        padding: 10,
        marginBottom: 10,
        backgroundColor: 'rgba(255,255,255,1)',
    },
    buttonContainer: {
        alignSelf: 'stretch',
        margin: 20,
        padding: 20,
        backgroundColor: 'blue',
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(255,255,255,0.6)',
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    }

})
export default AppLogin;