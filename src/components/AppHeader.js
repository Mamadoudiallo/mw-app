import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    TouchableWithoutFeedback,
    StyleSheet,
    Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');

const AppHeader = props => (
    <View  style={styles.backgroundImage}>
        <View style={styles.headerContainer}>
            <TouchableWithoutFeedback onPress={() => props.toggle()}>
                <Icon
                    name="bars"
                    color='white'
                    size={25}
                />
            </TouchableWithoutFeedback>
            <Image style={styles.logo} source={require('../img/logo.png')} />
            <Icon
                name="search"
                color='white'
                size={25}
            />
        </View>
    </View>
)
const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        marginTop:30,
        backgroundColor: "transparent"
    },
    logo: {
        width: 180,
        height: 41
    },
    backgroundImage: {
    backgroundColor: '#2196F3',
    width: width,
    height: 100,
  }
})
export default AppHeader;
