import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get('window');

class Menu extends Component {
    render() {
        return (
            <View style={styles.menu}>
                <View style={styles.avatarContainer}>
                    <View style={styles.avatarImage}>
                        <Image style={styles.avatar}
                            source={require('../img/mamadoudiallo100x100.jpg')}
                        />
                        <Text style={styles.text}>Mamadou</Text>
                    </View>
                    <Icon
                        name="exchange"
                        color="white"
                        size={25}
                    />
                </View>
                <ScrollView style={styles.scrollContainer}>
                    <View style={styles.textWithIcon}>
                        <View style={styles.withIcon}>
                            <Icon
                                style={styles.iconWithText}
                                name="users"
                                color="white"
                                size={28}
                            />
                            <Text style={styles.text}>Klassen</Text>
                        </View>
                        <Icon
                            style={styles.rightIcon}
                            name="angle-right"
                            color="white"
                            size={25}
                        />

                    </View>
                    <View style={styles.textWithIcon}>
                        <View style={styles.withIcon}>
                            <Icon
                                style={styles.iconWithText}
                                name="user"
                                color="white"
                                size={28}
                            />
                            <Text style={styles.text}>Gebruikers</Text>
                        </View>
                        <Icon
                            style={styles.rightIcon}
                            name="angle-right"
                            color="white"
                            size={25}
                        />

                    </View>
                    <View style={[styles.items, styles.itemSelected]}>
                        <Text style={styles.text}>Home</Text>
                    </View>
                    <View style={[styles.items, styles.noSelected]}>
                        <Text style={styles.text}>Home</Text>
                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        width: width,
        height: height,
        backgroundColor: "#0063B1"
    },
    avatarContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width / 2 + 59,
        borderColor: "#03A9F4",
        borderBottomWidth: 2,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    avatar: {
        width: 60,
        height: 60,
        marginRight: 20,
        borderRadius: 30
    },
    avatarImage: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        color: "white",
        fontSize: 16
    },
    textWithIcon: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: "#03A9F4",
        paddingVertical: 15,
        borderBottomWidth: 2,
    },
    withIcon: {
        flexDirection: "row",
        alignItems: "center"
    },
    scrollContainer: {
        width: width / 2 + 59
    },
    rightIcon: {
        paddingRight: 20
    },
    iconWithText: {
        paddingLeft: 20,
        paddingRight: 20
    },
    items:{
        paddingVertical:15,
        paddingLeft:20,
        marginTop:5
    },
    itemSelected:{
        borderLeftWidth:5,
        borderColor:"red"
    },
    noSelected:{
        paddingVertical:15,
        paddingLeft:25,
        marginTop:5
    },

})
export default Menu;