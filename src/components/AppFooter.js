import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

class AppFooter extends Component {

    render() {
        return (
            <View style={styles.appFooter}>
                <Text>Footer</Text>
            </View>
        );
    }
}

export default AppFooter;

const styles = StyleSheet.create({
    appFooter:{
        backgroundColor: '#E91E63',
        height: 40
    }
})