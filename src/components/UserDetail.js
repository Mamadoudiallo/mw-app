import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Image, Text } from 'react-native';

class UserDetail extends Component {
  static navigationOptions = {
    header: null,
};
  render() {
    const { picture, name, email, phone, login, dob, location } = this.props.navigation.state.params;
    return (
      <ScrollView>
        <View style={styles.hearderContainer}>
          <Image source={{ uri: picture.thumbnail }} style={styles.avatar} /> 
          <Text style={styles.usernameText}>{name.first} {name.last}</Text>
        </View>

        <View style={styles.workTimeContainer}>
          <View style={styles.homeworkView}>
              <Text style={styles.homeworkNumber}>40</Text>
              <Text style={styles.workingTextColor}>Huis tijd</Text>
          </View>
          <View style={styles.schoolView}>
              <Text style={styles.schoolNumber}>10</Text>
              <Text style={styles.workingTextColor}>School tijd</Text>
          </View>
        </View>

        <View style={styles.containerWrapper}>
          <View style={styles.contentWrapper}>
              <Text style={styles.textColor}>40</Text>
              <Text style={styles.textColor}>Huis tijd</Text>
          </View>
          <View style={styles.contentWrapper}>
              <Text style={styles.textColor}>10</Text>
              <Text style={styles.textColor}>School tijd</Text>
          </View>
        </View>
        <View style={styles.containerWrapper}>
          <View style={styles.contentWrapper}>
              <Text style={styles.textColor}>40</Text>
              <Text style={styles.textColor}>Huis tijd</Text>
          </View>
          <View style={styles.contentWrapper}>
              <Text style={styles.textColor}>10</Text>
              <Text style={styles.textColor}>School tijd</Text>
          </View>
        </View>
        <View style={styles.containerWrapper}>
          <View style={styles.contentWrapper}>
              <Text style={styles.textColor}>40</Text>
              <Text style={styles.textColor}>Huis tijd</Text>
          </View>
          <View style={styles.contentWrapper}>
              <Text style={styles.textColor}>10</Text>
              <Text style={styles.textColor}>School tijd</Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default UserDetail;

const styles = StyleSheet.create({
  hearderContainer: {
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'flex-start',
    backgroundColor: '#3F51B5',
    padding:20,
    height: 250,
  },
  avatar: {
    height: 150,
    width: 150,
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 75,
  },
  usernameText: {
    color: 'white',
    fontSize: 30,
    paddingLeft: 20,
  },
  workingTextColor:{
    color: 'white',
    fontSize: 20,
  },
  textColor: {
    color: 'grey',
    fontSize: 20,
    padding: 10,
  },
  homeworkNumber: {
    color: 'white',
    fontSize: 40,
  },
  schoolNumber: {
    color: 'white',
    fontSize: 40,
  },
  homeworkSubTitle: {
    color: 'white',
    fontSize: 16,
  },
  schoolSubTitle: {
    color: 'white',
    fontSize: 16,
  },
  homeworkView: {
    flex: 1,
    backgroundColor: '#64B5F6',
    alignItems: 'center',
    justifyContent: 'center',
  },
  schoolView: {
    flex: 1,
    backgroundColor: '#FF7FE5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  workTimeContainer: {
    flex: 1,
    backgroundColor: "#03A9F4",
    flexDirection: 'row',
    height:120,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  containerWrapper: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    marginTop: 10,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da'

  },
  contentWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: 'lightgrey',
  },
});