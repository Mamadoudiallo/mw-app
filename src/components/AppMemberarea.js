import React, { Component } from 'react';
import {
    View
} from 'react-native';
import SideMenu from 'react-native-side-menu';

import AppHeader from './AppHeader';
import Menu from './Menu';
import CLassList from './CLassList';

class AppMemberarea extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
    }
    static navigationOptions = {
        header: null,
    };
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    updateMenu(isOpen) {
        this.setState({ isOpen })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <SideMenu
                    menu={<Menu />}
                    isOpen={this.state.isOpen}
                    onChange={(isOpen) => this.updateMenu(isOpen)} >
                    <AppHeader toggle={this.toggle.bind(this)} />
                    <CLassList  navigation = {this.props.navigation} />
                </SideMenu>
            </View>
        );
    }
}

export default AppMemberarea;