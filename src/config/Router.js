import React, {
    Component
} from 'react';

import {StackNavigator} from 'react-navigation';

import AppLogin from './../components/AppLogin';
import AppMemberarea from './../components/AppMemberarea';
import UserDetail from './../components/UserDetail';
import UsersList from './../components/UsersList';

export const LoginStack = StackNavigator({
    Login: {screen: AppLogin},
    Memberarea: {screen: AppMemberarea},
    Details: {screen: UserDetail},
    UsersScreen: {screen: UsersList}
});

export const Root = StackNavigator({
    AppLogin: {
        screen: LoginStack,
    },
}
);